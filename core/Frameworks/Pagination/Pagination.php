<?php
namespace Core\Frameworks\Pagination;

use \Core\Frameworks\FrameworkBase;

class Pagination
{	
	protected $per_page_limit = 0;
	protected $total_row = 0;
	protected $total_link = 0;
	protected $page = 0;
	protected $data = [];
	protected $path = '';
	protected $full_link = '';
	protected $max_link_display = 8; // <first ... 2 . 3 . 4 . 5 . 6 ... last>

	function __construct()
	{

	}

	function setup($config = [])
	{
		$this->per_page_limit = $config['per_page_limit'];
		$this->total_row = $config['total_row'];
		$this->total_link = $this->construct_total_link();
		$this->page = $config['page'];
		$this->additional_variable = !empty($config['additional_variable']) ? $config['additional_variable'] : '';
		$this->data = $this->convert_raw_data($config['data']);
		$this->path = $config['path'];
		$this->full_link = $this->construct_link();

		return $this->full_link;
	}

	function construct_link()
	{
		$set_number = $this->max_link_display / 2;
     	$set_link_explode = explode('.', $set_number);
     	$pervious_set_number = [];
     	$next_set_number = [];
     	$this->pervious_set_number_load = !empty($set_link_explode[1]) ? $set_number - (floatval('0.'.$set_link_explode[1])) : $set_number;
     	$this->next_set_number_load = !empty($set_link_explode[1]) ? $set_number - (floatval('0.'.$set_link_explode[1])) + 1 : $set_number;

     	//pervious set 
     	$pervious_set_array = [];
     	$pervious_add = $this->next_set_number_load - ($this->total_link - $this->page);
     	$pervious_add = $pervious_add < 0 ? 0 : $pervious_add;
     	for ($i = 0; $i < $this->pervious_set_number_load + $pervious_add ; $i++) { 
     		$pervious_page_number = $this->page - $i;
     		if ($pervious_page_number > 0) {
     			$pervious_set_number[$pervious_page_number] = $pervious_page_number;
     			$page_text = $pervious_page_number;
     			$pervious_set_array[] = $this->build_path($pervious_page_number - 1, $page_text);
     		}
     	}
     	$pervious_set_link = implode(' ', array_reverse($pervious_set_array));

     	//next set 
     	$next_set_array = [];
     	$next_add = $this->pervious_set_number_load - (($this->page + $this->total_link) - $this->total_link);
     	$next_add = $next_add < 0 ? 0 : $next_add;
     	for ($i = 1; $i < $this->next_set_number_load + 1 + $next_add; $i++) { 
     		$next_page_number = $this->page + $i;
     		if ($next_page_number <= $this->total_link) {
     			$next_set_number[$next_page_number] = $next_page_number;
     			$page_text = $next_page_number;
     			$next_set_array[] = $this->build_path($next_page_number, $page_text + 1);
     		}
     	}
     	$next_set_link = implode(' ', $next_set_array);

     	//next link
        $next_link_number = $this->page + 1;
        $next_link = $this->construct_exists_path($next_link_number);
        if (!empty($next_link)) {
			$next_link = $this->build_path($page_number = $next_link_number, $page_text = '>>');
        }

        //pervious link
        $pervious_link_number = $this->page - 1;
        $pervious_link = $this->construct_exists_path($pervious_link_number);
        if (!empty($pervious_link)) {
			$pervious_link = $this->build_path($page_number = $pervious_link_number, $page_text = '<<');
        }

        //first link
	    $first_link = '';
        if (!in_array(1, $pervious_set_number) && !empty($pervious_set_number)) {
	        $first_link_number = 0;
			$first_link = $this->build_path($page_number = $first_link_number, $page_text = 'first', $add_class = ' first_path');
		}

		//last link
	    $last_link = '';
        if (!in_array($this->total_link, $next_set_number) && !empty($next_set_number)) {
	        $last_link_number = $this->total_link;
			$last_link = $this->build_path($page_number = $last_link_number, $page_text = 'last', $add_class = ' last_path');
		}

		//active page
		$active_page = $this->build_path($page_number = $this->page, $page_text = ($this->page + 1), $add_class = ' active');
		$full_link = 
					'<ul class="pagination pull-right">' .
						$first_link .
						$pervious_link .
						$pervious_set_link .
						$active_page .
						$next_set_link .
						$next_link . 
						$last_link .
					'</ul>';

		return $full_link;
	}

	function construct_total_link()
	{
		$total_link = $this->total_row / $this->per_page_limit;
        $total_link_explode = explode('.', $total_link);
        if (!empty($total_link_explode[1])) {
            $total_link = ($total_link - (floatval('0.'.$total_link_explode[1])));
        }
        return $total_link;
	}

	function construct_exists_path($set = 0)
	{
		$path = '';
		$page_text = $set + 1;
		if ($set <= $this->total_link && $page_text > 0) {
			$path = $this->build_path($page_number = $set, $page_text = $page_text);

		}
		return $path;
	}

	function build_path($page_number = 0, $page_text = '', $add_class = '')
	{
		$offset = (int) $this->per_page_limit * $page_number;
		$extra_path = '&offset=' . $offset;
		$extra_path .= '&limit=' . $this->per_page_limit;
		$extra_path .= !empty($this->additional_variable) ? '&' . http_build_query($this->additional_variable) : '';
		return $path = '<li class="link_path' . $add_class . '"><a href=" '. $this->path . '?page=' . $page_number . $extra_path . '  ">' . $page_text . '</a></li>';
	}

	function convert_raw_data($data = [])
	{


	}
	
}


  