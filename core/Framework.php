<?php
// this class design to call framework 

namespace Core;

class Framework
{
	protected $framework_load = [];
	
	function __construct()
	{
		$this->set_framework();
		$this->load_framework();
	}

	function set_framework()
	{
		$this->framework_load = FRAMEWORK;
	}
	
	function load_framework()
	{
		foreach ($this->framework_load as $key => $row) {
			$class = ucfirst($row);
			$framework_call = 'Core\Frameworks\\' . $class . '\\' . $class;
			$this->$row = new $framework_call();
		}
	}
}