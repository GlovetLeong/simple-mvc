<?php
namespace Core\General;

class Language
{
	protected $file = '';
	protected $string = '';

	function __construct()
	{
		
	}

	function getLanguageFile($lang){
		$languageData = [];

		$data = file_get_contents(BASE_URL . 'app/lang/' . $lang . '/file.txt');
		$data = preg_split("/[\n]+/", $data, -1, PREG_SPLIT_NO_EMPTY);

		foreach ($data as $value) {
			$values = explode("=>", $value, 2);
			if (!empty($values[1])) {
				$languageData[$values[0]] = $values[1];
			}
		}
	
		return $languageData;
	}

	function tran($string = '', $load_lang = '')
	{
		$load_lang = !empty($this->load_lang) ? $this->load_lang : $load_lang;
		$this->file = $this->getLanguageFile($load_lang);
		return !empty($this->file[$string]) ? trim($this->file[$string]) : 'text not found (' . $string . ')';
	}


}