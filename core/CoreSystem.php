<?php
namespace Core;

use \Core\General\Request;
use \Core\General\Language;

use App;

class CoreSystem
{
	protected $request;
	protected $frame;
	protected $validation;
	protected $lang;

	function __construct()
	{
		//use GLOBALS to load one times

		//load general functions
		if (empty($GLOBALS['request'])) {
			$GLOBALS['request'] = new Request;
		}
		$this->request = $GLOBALS['request'];

		if (empty($GLOBALS['lang'])) {
			$GLOBALS['lang'] = new Language;
		}
		$this->lang = $GLOBALS['lang'];

		//load framework
		if (empty($GLOBALS['frame'])) {
			$GLOBALS['frame'] = new Framework;
		}
		$this->frame = $GLOBALS['frame'];

		//shorten framework name
		foreach (FRAMEWORK as $key => $row) {
			if ($row == 'validation') {
				$this->validation = $this->frame->validation;
			}
			if ($row == 'pagination') {
				$this->pagination = $this->frame->pagination;
			}
		}
	}

	function call($controller = '')
	{	
		$controller_define = explode('@', rtrim($controller, '/'));
		$controllerName = ucfirst($controller_define[0]);
		$functionName = $controller_define[1];
		$call = 'App\Controllers\\' . $controllerName;
		$controller = new $call;
		$controller->$functionName();
	}
}